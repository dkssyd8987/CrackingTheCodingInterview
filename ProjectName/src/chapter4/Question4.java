package chapter4;

public class Question4 {
	public <T> boolean checkBalanced(CJBNode<T> node) {
		if(node == null) {
			return true;
		}
		int diff = getHeight(node.getFirstChild()) - getHeight(node.getLastChild());
		System.out.println("diff : " + diff);
		if(Math.abs(diff) > 1) {
			return false;
		} else {
			return checkBalanced(node.getFirstChild()) && checkBalanced(node.getLastChild());
		}
	}
	public <T> int getHeight(CJBNode<T> node) {
		if(node == null) {
			return -1;
		}
		return Math.max(getHeight(node.getFirstChild()), getHeight(node.getLastChild())) + 1;
	}
	public static void main(String[] args) {
		Question4 obj = new Question4();
		CJBTree<Integer> test = new CJBTree<Integer>();
		for(int i = 0; i < 10; i++) {
			test.insertNode(new CJBNode<Integer>(i));
		}
		test.getRoot().getFirstChild().getFirstChild().getFirstChild().addChild(new CJBNode<Integer>(100));
		test.getRoot().getFirstChild().getFirstChild().getFirstChild().getFirstChild().addChild(new CJBNode<Integer>(100));
		System.out.println(obj.checkBalanced(test.getRoot()));
		test.dPrintTree();
	}
}
