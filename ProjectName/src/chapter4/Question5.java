package chapter4;

public class Question5 {
	public boolean checkBST(CJBNode<Integer> root) {
		if(root == null) {
			return true;
		}
		return checkRightOrder(root);
	}
	public boolean checkRightOrder(CJBNode<Integer> node) {
		if(node == null) {
			return true;
		}
		if(node.getFirstChild() == null) {
			return true;
		}
		if(node.getFirstChild().getItem() > node.getItem()) {
			return false;
		}
		if(node.getLastChild().getItem() < node.getItem()) {
			return false;
		}
		return checkRightOrder(node.getFirstChild()) && checkRightOrder(node.getLastChild());
	}
	public static void main(String[] args) {
		Question5 obj = new Question5();
		CJBTree<Integer> test = new CJBTree<Integer>();
		CJBTree<Integer> test2 = new CJBTree<Integer>();
		Integer[] array = new Integer[10];
		for(int i = 0; i < 10; i++) {
			test.insertNode(new CJBNode<Integer>(i));
		}
		test2.insertNode(new CJBNode<Integer>(5));
		test2.insertNode(new CJBNode<Integer>(1));
		test2.insertNode(new CJBNode<Integer>(8));
		test2.insertNode(new CJBNode<Integer>(0));
		test2.insertNode(new CJBNode<Integer>(3));
		System.out.println(obj.checkBST(test2.getRoot()));
		System.out.println(obj.checkBST(test.getRoot()));
		test.dPrintTree();
	}
}
