package chapter4;

import java.util.LinkedList;

import javax.print.attribute.standard.PrinterLocation;

public class Question1 {
	public boolean checkRoute(CJBGraph graph, CJBGraph.Node first, CJBGraph.Node second) {
		LinkedList<CJBGraph.Node> ll = new LinkedList<CJBGraph.Node>();
		first.setMark(true);
		ll.add(first);
		while(!ll.isEmpty()) {
			CJBGraph.Node n = ll.removeFirst();
			if(n != null) {
				for (CJBGraph.Node node : n.getAdjacent()) {
					if(node != null) {
						if(node.getMark() == false) {
							n.setMark(true);
							ll.add(node);
						}
						if(node == second) {
							return true;
						}
					}
				}
			}
		}
		return false;
	}
	public static void main(String[] args) {
		Question1 obj = new Question1();
		CJBGraph g = new CJBGraph();
		CJBGraph.Node n1 = g.new Node(1);
		CJBGraph.Node n2 = g.new Node(2);
		CJBGraph.Node n3 = g.new Node(3);
		n1.setAdjacent(n2);
		System.out.println(obj.checkRoute(g, n1, n3));
	}
}

