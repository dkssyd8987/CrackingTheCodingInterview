package chapter4;

public class CJBGraph {
	public class Node {
		private int item;
		private Node[] adjacentNodes = new Node[3];
		private int adjacentSize;
		private boolean mark;
		public Node(int item) {
			this.item = item;
			adjacentSize = 0;
			mark = false;
		}
		public Node[] getAdjacent() {
			return adjacentNodes;
		}
		public void setAdjacent(Node n) {
			adjacentNodes[adjacentSize] = n;
			adjacentSize++;
		}
		public void setMark(boolean b) {
			mark = b;
		}
		public boolean getMark() {
			return mark;
		}
	}
	private Node[] nodes;
	private int nodeSize = 0;
	public void addNodeWithItem(int item) {
		nodes[nodeSize] = new Node(item);
		nodeSize++;
	}
	public void addNode(Node n) {
		nodes[nodeSize] = n;
		nodeSize++;
	}
}
