package chapter4;

import java.util.LinkedList;

public class Question7 {
	public <T> LinkedList<CJBNode<T>> makeOrder(LinkedList<CJBNode<T>> ll) {
		int[] array = new int[ll.size()];
		for(int i = 0; i < ll.size(); i++) {
			array[i] = countDependency(ll.get(i));
			System.out.println(ll.get(i).getItem());
		}
		LinkedList<CJBNode<T>> newLL = new LinkedList<CJBNode<T>>();
		int maxDependency = 0;
		for(int i = 0; i < array.length; i++) {
			if(array[i] > maxDependency) {
				maxDependency = array[i];
			}
		}
		for(int i = 0; i <= maxDependency; i++) {
			for(int j = 0; j < array.length; j++) {
				if(array[j] == i) {
					newLL.add(ll.get(j));
				}
			}
		}
		return newLL;
	}
	public <T> int countDependency(CJBNode<T> node) {
		if(node == null || node.getAdjacentNodes().size() == 0) {
			return 0;
		}
		int max = 0;
		for(CJBNode<T> n : node.getAdjacentNodes()) {
			if(max < countDependency(n) + 1) {
				max = countDependency(n) + 1;
			}
		}
		return max;
	}
	public <T> void setDependency(CJBNode<T> first, CJBNode<T> second) {
		second.setAdjacentNode(first);
	}
	public static void main(String[] args) {
		Question7 obj = new Question7();
		LinkedList<CJBNode<String>> test = new LinkedList<CJBNode<String>>();
		CJBNode<String> aNode = new CJBNode<String>("a");
		CJBNode<String> bNode = new CJBNode<String>("b");
		CJBNode<String> cNode = new CJBNode<String>("c");
		CJBNode<String> dNode = new CJBNode<String>("d");
		CJBNode<String> eNode = new CJBNode<String>("e");
		CJBNode<String> fNode = new CJBNode<String>("f");
		test.add(aNode);
		test.add(bNode);
		test.add(cNode);
		test.add(dNode);
		test.add(eNode);
		test.add(fNode);
		obj.setDependency(aNode, bNode);
		obj.setDependency(fNode, bNode);
		obj.setDependency(bNode, dNode);
		obj.setDependency(fNode, aNode);
		obj.setDependency(dNode, cNode);
		for(CJBNode<String> node : obj.makeOrder(test)) {
			System.out.println(node.getItem());
		}
	}
}
