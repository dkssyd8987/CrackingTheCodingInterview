package chapter4;

import java.util.LinkedList;

public class Question8 {
	public <T> CJBNode<T> getCommonAncestor(CJBNode<T> first, CJBNode<T> second) {
		CJBNode<T> commonAncestor = null;
		if(first.getDepth() > second.getDepth()) {
			while(true) {
				first = first.getParent();
				if(first.getDepth() == second.getDepth()) {
					break;
				}
			}
		}
		else if(first.getDepth() < second.getDepth()) {
			while(true) {
				second = second.getParent();
				if(first.getDepth() == second.getDepth()) {
					break;
				}
			}
		}
		CJBNode<T> firstParent = first.getParent();
		CJBNode<T> secondParent = second.getParent();
		while(firstParent != null) {
			if(firstParent == secondParent) {
				commonAncestor = firstParent;
				break;
			}
			firstParent = firstParent.getParent();
			secondParent = secondParent.getParent();
		}
		return commonAncestor;
	}
	public static void main(String[] args) {
		Question8 obj = new Question8();
		LinkedList<CJBNode<Integer>> testLL = new LinkedList<CJBNode<Integer>>();
		CJBTree<Integer> test = new CJBTree<Integer>();
		for(int i = 0; i < 30; i++) {
			CJBNode<Integer> node = new CJBNode<Integer>(i);
			testLL.add(node);
			test.insertNode(node);
		}
		test.dPrintTree();
		System.out.println(obj.getCommonAncestor(testLL.get(3),  testLL.get(18)).getItem());
	}
}
