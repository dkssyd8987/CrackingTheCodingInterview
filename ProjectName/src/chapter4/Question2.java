package chapter4;

public class Question2 {
	public <T> CJBTree<T> makeBST(T[] array) {
		CJBTree<T> tree = new CJBTree<T>();
		insertMiddleValue(tree,  0,  array.length -1,  array);
		return tree;
	}
	public <T> CJBNode<T> insertMiddleValue(CJBTree<T> tree, int startIdx, int endIdx, T[] array) {
		if(endIdx < startIdx) {
			return null;
		}
		int mid = (startIdx + endIdx) / 2;
		CJBNode<T> node = new CJBNode<T>(array[mid]);
		if(tree.getRoot() == null) {
			tree.setRoot(node);
		}
		node.addChild(insertMiddleValue(tree, startIdx, mid - 1, array));
		node.addChild(insertMiddleValue(tree,  mid + 1,  endIdx, array));
		return node;
	}
	public static void main(String[] args) {
		Question2 obj = new Question2();
		Integer[] array = new Integer[11];
		for(int i = 0; i < 11; i++) {
			array[i] = i;
		}
		CJBTree<Integer> test = obj.makeBST(array);
		test.dPrintTree();
	}
}
