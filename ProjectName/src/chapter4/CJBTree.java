package chapter4;

import java.util.LinkedList;

public class CJBTree<T> {
	private CJBNode<T> root;
	private CJBNode<T> tempNode;
	public void setRoot(CJBNode<T> n) {
		this.root = n;
	}
	public CJBNode<T> getRoot() {
		return this.root;
	}
	public void insertNode(CJBNode<T> n) {
		if(this.root == null) {
			this.root = n;
			return;
		}
		CJBNode<T> searchNode = this.root;
		LinkedList<CJBNode<T>> ll = new LinkedList<CJBNode<T>>();
		ll.add(searchNode);
		while(!ll.isEmpty()) {
			CJBNode<T> r = ll.removeFirst();
			if(r.getChildren().size() < 2) {
				r.addChild(n);
				return;
			}
			for(CJBNode<T> node : r.getChildren()) {
				if(node != null) {
					ll.add(node);
				}
			}
		}
	}
	public void swap(CJBNode<T> parent, CJBNode<T> child) {
		if(child.getParent() != parent) {
			System.out.println("wrong parent");
			return;
		}
		LinkedList<CJBNode<T>> tempChildren = parent.getChildren();
		CJBNode<T> tempParent = parent.getParent();
		parent.setChildren(child.getChildren());
		parent.setParent(child);
		child.setChildren(tempChildren);
		child.setParent(tempParent);
	}
	public void dPrintTree() {
		if(this.root == null) {
			return;
		}
		LinkedList<CJBNode<T>> ll = new LinkedList<CJBNode<T>>();
		ll.add(this.root);
		System.out.println(this.root + " root " + this.root.getItem() + "depth" + this.root.getDepth());
		while(!ll.isEmpty()) {
			CJBNode<T> r = ll.removeFirst();
			System.out.println("depth: " + r.getDepth() + "value: " + r.getItem());
			for(CJBNode<T> node : r.getChildren()) {
				if(node != null) {
					ll.add(node);
				}
			}
		}
	}
}