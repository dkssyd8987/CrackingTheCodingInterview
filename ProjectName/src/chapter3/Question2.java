package chapter3;

import java.util.EmptyStackException;

public class Question2 {
	private class minStack {
		private class StackNode {
			private int data;
			private StackNode next;
			private StackNode prvMin;
			
			public StackNode(int data) {
				this.data = data;
			}
		}
		
		private StackNode top;
		private StackNode min;
		
		public int pop() {
			if(top == null) throw new EmptyStackException();
			if(top == min) {
				min = min.prvMin;
			}
			int item = top.data;
			top = top.next;
			return item;
		}
		
		public void push(int item) {
			StackNode t = new StackNode(item);
			if(min == null) {
				min = t;
			}
			else if(min.data > t.data) { 
				t.prvMin = min;
				min = t;
			}
			t.next = top;
			top = t;
		}
		
		public int peek() {
			if (top == null) throw new EmptyStackException();
			return top.data;
		}
		
		public int min() {
			if (min == null) throw new EmptyStackException();
			return min.data;
		}
	}
	public static void main(String[] args) {
		Question2 obj = new Question2();
		minStack test = obj.new minStack();
//		test.push(3);
		test.push(1);
		test.push(5);
		test.push(4);
		test.pop();
		System.out.println(test.min());
	}
}
