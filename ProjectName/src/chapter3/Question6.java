package chapter3;

import java.util.LinkedList;

public class Question6 {
	class Animal {
		protected int age;
		protected String kind;
		
		public void setKind(String kind) {
			this.kind = kind;
		}
		
		public void setAge(int age) {
			this.age = age;
		}
		
		public int getAge() {
			return this.age;
		}
		
		public String getKind() {
			return this.kind;
		}
		
		protected boolean isDog() {
			return false;
		}
		
		protected boolean isCat() {
			return false;
		}
	}
	
	class Dog extends Animal {
		public boolean isDog() {
			return true;
		}
	}
	
	class Cat extends Animal {
		public boolean isCat() {
			return true;
		}
	}
	
	LinkedList<Dog> dog = new LinkedList<Dog>();
	LinkedList<Cat> cat = new LinkedList<Cat>();
	
	private int age = 0;
	
	public void enqueue(Animal animal) {
		if(animal.isDog()) {
			animal.setAge(this.age);
			animal.setKind("Dog");
			this.age++;
			dog.add((Dog)animal);
		}
		else if(animal.isCat()) {
			animal.setAge(this.age);
			animal.setKind("Cat");
			this.age++;
			cat.add((Cat)animal);
		}
	}
	
	public Animal dequeueAny() {
		if(cat.peekFirst() == null) {
			return dog.pollFirst();
		}
		else if(dog.peekFirst() == null) {
			return cat.pollFirst();
		}
		if(cat.peekFirst().getAge() < dog.peekFirst().getAge())
		{
			return cat.pollFirst();
		}
		else {
			return dog.pollFirst();
		}
		
	}
	
	public Dog dequeueDog() {
		return dog.pollFirst();
	}
	
	public Cat dequeueCat() {
		return cat.pollFirst();
	}
	
	public static void main(String[] args) {
		Question6 obj = new Question6();
		Cat cat1 = obj.new Cat();
		Cat cat2 = obj.new Cat();
		Dog dog1 = obj.new Dog();
		Dog dog2 = obj.new Dog();
		obj.enqueue(dog1);
		obj.enqueue(cat1);
		obj.enqueue(cat2);
		obj.enqueue(dog2);
		System.out.println(obj.dequeueAny());
		System.out.println(obj.dequeueAny());
		System.out.println(obj.dequeueAny());
		System.out.println(obj.dequeueAny());
	}
}
