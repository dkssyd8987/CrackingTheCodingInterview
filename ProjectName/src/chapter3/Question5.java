package chapter3;

import chapter3.Question3.setOfStacks;

public class Question5 {
	public void sort(CJBStack<Integer> stack) {
		CJBStack<Integer> tmpStack = new CJBStack<Integer>();
		while(stack.isEmpty() == false) {
			Integer temp = stack.pop();
			while((tmpStack.isEmpty() == false) && (tmpStack.peek() > temp)) {
				stack.push(tmpStack.pop());
			}
			tmpStack.push(temp);
		}
		while(tmpStack.isEmpty() == false) {
			stack.push(tmpStack.pop());
		}
	}
	
	public static void main(String[] args) {
		Question5 obj = new Question5();
		CJBStack<Integer> test = new CJBStack<Integer>();
		test.push(1);
		test.push(4);
		test.push(2);
		test.push(8);
		test.push(5);
		while(test.isEmpty() == false) {
			System.out.println(test.pop());
		}
		test.push(1);
		test.push(4);
		test.push(2);
		test.push(8);
		test.push(5);
		obj.sort(test);
		while(test.isEmpty() == false) {
			System.out.println(test.pop());
		}
	}
}
