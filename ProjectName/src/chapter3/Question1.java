package chapter3;

import java.util.EmptyStackException;

public class Question1 {
	private class arrayStacks<T> {
		private T[] array;
		private int bnd;
		
		private int id1;
		private int id2;
		private int id3;
		
		public void setArray(T[] array) {
			this.array = array;
			bnd = array.length/3;
			id1 = 0;
			id2 = bnd;
			id3 = bnd*2;
		}
		
		public T pop(int index) {
			switch(index) {
			case 1: 
				if(id1-1 < 0) throw new EmptyStackException();
				T item = array[id1];
				id1--;
				return item;
			case 2:
				if(id2-1 < bnd) throw new EmptyStackException();
				T item2 = array[id2];
				id2--;
				return item2;
			case 3:
				if(id3-1 < 2*bnd) throw new EmptyStackException();
				T item3 = array[id3];
				id3--;
				return item3;
			}
			throw new EmptyStackException();
		}
		
		public void push(T item, int index) {
			switch(index) {
			case 1:
				id1++;
				array[id1] = item;
				break;
			case 2:
				id2++;
				array[id2] = item;
				break;
			case 3:
				id3++;
				array[id3] = item;
				break;
			}
		}
		
		public T peek(int index) {
			switch(index) {
			case 1:
				return array[id1];
			case 2:
				return array[id2];
			case 3:
				return array[id3];
			}
			throw new EmptyStackException();
		}
	}
	public static void main(String[] args) {
		Question1 obj = new Question1();
		arrayStacks test = obj.new arrayStacks();
		Integer[] array = new Integer[9];
		test.setArray(array);
		test.push(1, 1);
		test.push(2,  1);
		test.push(3, 1);
		test.push(10, 2);
		test.push(100, 3);
		test.pop(1);
		test.pop(1);
		System.out.println(test.peek(1));
		System.out.println(test.pop(2));
	}
}
