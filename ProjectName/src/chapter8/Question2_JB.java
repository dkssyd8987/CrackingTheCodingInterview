package chapter8;

import java.util.ArrayList;

public class Question2_JB {
	public class Node {
		private int row = -1;
		private int col = -1;
		private boolean offLimit = false;
		
		public Node(int row, int col) {
			this.row = row;
			this.col = col;
		}
		
		public void setOfflimit() {
			this.offLimit = true;
		}
		
		public boolean getOfflimit() {
			return this.offLimit;
		}
		
		public int getRow() {
			return this.row;
		}
		
		public int getCol() {
			return this.col;
		}
	}
	
	public class Grid {
		private Node[][] nodeList = null;
		public Grid(int row, int col) {
			this.nodeList = new Node[row][col];
			for(int i = 0; i < row; i++) {
				for(int j = 0; j < col; j++) {
					this.nodeList[i][j] = new Node(i, j);
				}
			}
		}
		
		public void setOffLimit(int row, int col) {
			this.nodeList[row][col].setOfflimit();
		}
		
		public Node getLastNode() {
			return this.nodeList[this.nodeList.length-1][this.nodeList[0].length-1];
		}
		
		public Node getLeftNode(Node node) {
			return this.nodeList[node.getRow()][node.getCol()-1];
		}
		
		public Node getUpsideNode(Node node) {
			return this.nodeList[node.getRow()-1][node.getCol()];
		}
	}
	
	public ArrayList<String> getPossiblePaths(Grid grid) {
		return getPossiblePaths(grid, grid.getLastNode());
	}
	
	public ArrayList<String> getPossiblePaths(Grid grid, Node node) {
		// 왼쪽 위를 (0,0) 으로 잡고 오른쪽으로 움직이면 (0,1) 아래로 움직이면 (1,0)
		if(node.getOfflimit() == true) {
			// offlimit 인 node 에서는 경로가 없는 것이나 다름 없으므로 빈 list 를 return
			ArrayList<String> list = new ArrayList<String>();
			return list;
		}
		if(node.getRow() == 0) {
			if(node.getCol() == 1) {
				// (0,1) 인 경우 오른쪽으로 하나 오는 경로밖에 없으므로 "R"
				ArrayList<String> list = new ArrayList<String>();
				list.add("R");
				return list;
			}
			else {
				// (0,x) 인 경우 왼쪽으로만 움직이면서 경로 수집
				// (0,x-1) 의 경로 list 를 받아서 각각의 뒤에 R 을 붙이면 (0,x) 로의 경로 list
				ArrayList<String> oldList = getPossiblePaths(grid, grid.getLeftNode(node));
				ArrayList<String> list = new ArrayList<String>();
				for(String s : oldList) {
					list.add(s + "R");
				}
				return list;
			}
		}
		else if(node.getCol() == 0) {
			if(node.getRow() == 1) {
				// (1,0) 인 경우 아래로 하나 오는 경로밖에 없으므로 "D"
				ArrayList<String> list = new ArrayList<String>();
				list.add("D");
				return list;
			}
			else {
				// (x,0) 인 경우 위로만 움직이면서 경로 수집
				// (x-1, 0) 의 경로 list 를 받아서 각각의 뒤에 D 를 붙이면 (x,0) 으로의 경로 list
				ArrayList<String> oldList = getPossiblePaths(grid, grid.getUpsideNode(node));
				ArrayList<String> list = new ArrayList<String>();
				for(String s : oldList) {
					list.add(s + "D");
				}
				return list;
			}
		}
		else {
			// (x,y) 인 경우 (x-1,y) 와 (x, y-1) 의 경로를 합쳐야 함
			// (x-1,y) 로부터는 + D, (x, y-1) 로부터는 + R 해서 모두 합치면 됨.
			ArrayList<String> leftList = getPossiblePaths(grid, grid.getLeftNode(node));
			ArrayList<String> upList = getPossiblePaths(grid, grid.getUpsideNode(node));
			ArrayList<String> list = new ArrayList<String>();
			for(String s : leftList) {
				list.add(s + "R");
			}
			for(String s : upList) {
				list.add(s + "D");
			}
			return list;
		}
	}
	
	public static void main(String[] args) {
		Question2_JB obj = new Question2_JB();
		Grid grid = obj.new Grid(4, 4);
		grid.setOffLimit(2, 3);
		grid.setOffLimit(2, 2);
		ArrayList<String> list = obj.getPossiblePaths(grid);
		System.out.println(list);
	}
}
