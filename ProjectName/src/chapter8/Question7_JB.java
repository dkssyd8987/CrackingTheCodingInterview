package chapter8;

import java.util.ArrayList;

public class Question7_JB {
	public ArrayList<String> getEveryPermutation(String str) {
		ArrayList<String> list = new ArrayList<String>();
		return getEveryPermutation(str, list);
	}
	
	public ArrayList<String> getEveryPermutation(String str, ArrayList<String> list) {
		// 글자수가 1개면 그것만 list 에 포함시켜서 return
		if(str.length() == 1) {
			list.add(str);
			return list;
		}
		else {
			// 글자수가 2개 이상이면 맨 앞자리 글자를 나머지 자리로 만든 list 의 모든 자리에 끼워넣는다.
			String target = str.substring(0,1);
			String remained = str.substring(1);
			return pushInEveryPos(target, getEveryPermutation(remained, list));
		}
	}
	
	public ArrayList<String> pushInEveryPos(String c, ArrayList<String> list) {
		// 하나의 글자를 기존의 String 의 모든 자리에 끼워넣어서 새로운 list 를 만든다
		ArrayList<String> newList = new ArrayList<String>();
		for(String s : list) {
			for(int i = 0; i <= s.length(); i++) {
				newList.add(s.substring(0, i) + c + s.substring(i));
			}
		}
		return newList;
	}
	
	public static void main(String[] args) {
		Question7_JB obj = new Question7_JB();
		String str = "abcde";
		ArrayList<String> newList = obj.getEveryPermutation(str);
		System.out.println(newList.size());
		System.out.println(newList);
	}
}
