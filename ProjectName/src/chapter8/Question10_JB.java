package chapter8;

import java.util.ArrayList;

public class Question10_JB {
	public class Pixel {
		private int x = -1;
		private int y = -1;
		private String color = "None";
		
		public Pixel(int x, int y) {
			this.x = x;
			this.y = y;
		}
		
		public void setColor(String color) {
			this.color = color;
		}
		
		public String getColor() {
			return this.color;
		}
	}
	
	public class Canvas {
		private int width = -1;
		private int height = -1;
		private Pixel[][] pixels = null;
		
		public Canvas(int width, int height) {
			this.width = width;
			this.height = height;
			this.pixels = new Pixel[width][height];
			for(int i = 0; i < width; i++) {
				for(int j = 0; j < height; j++) {
					pixels[i][j] = new Pixel(i, j);
				}
			}
		}
		
		public int getWidth() {
			return this.width;
		}
		
		public int getHeight() {
			return this.height;
		}
		
		public Pixel getPixelFromCanvas(int x, int y) {
			if((x < 0) || (x > width-1)) {
				return null;
			}
			if((y < 0) || (y > height-1)) {
				return null;
			}
			return this.pixels[x][y];
		}
	}
	
	public void paintCanvasColor(Canvas canvas, String color) {
		int width = canvas.getWidth();
		int height = canvas.getHeight();
		paintCanvasColor(canvas, color, width/2, height/2);
	}
	
	public void paintCanvasColor(Canvas canvas, String color, int x, int y) {
		Pixel target = canvas.getPixelFromCanvas(x, y);
		if(target != null) {
			if(target.getColor() != color) {
				// 주변의 8개 위치에 대해서 canvas color 바꿔주는 걸 반복
				target.setColor(color);
				paintCanvasColor(canvas, color, x-1, y-1);
				paintCanvasColor(canvas, color, x-1, y);
				paintCanvasColor(canvas, color, x-1, y+1);
				paintCanvasColor(canvas, color, x, y-1);
				paintCanvasColor(canvas, color, x, y+1);
				paintCanvasColor(canvas, color, x+1, y-1);
				paintCanvasColor(canvas, color, x+1, y);
				paintCanvasColor(canvas, color, x+1, y+1);
			}
		}
	}
	
	public void printCanvas(Canvas canvas) {
		for(int i = 0; i < canvas.getHeight(); i++) {
			for(int j = 0; j < canvas.getWidth(); j++) {
				Pixel pixel = canvas.getPixelFromCanvas(j, i);
				System.out.print(pixel.getColor() + "   ");
			}
			System.out.println();
		}
	}
	
	public static void main(String[] args) {
		Question10_JB obj = new Question10_JB();
		Canvas canvas = obj.new Canvas(4, 4);
		obj.printCanvas(canvas);
		obj.paintCanvasColor(canvas,  "yellow");
		obj.printCanvas(canvas);
	}
}
