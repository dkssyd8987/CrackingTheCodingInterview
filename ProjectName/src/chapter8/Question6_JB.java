package chapter8;

import java.util.Arrays;
import java.util.Stack;

public class Question6_JB {
	class Tower {
		private Stack<Integer> disks;
		public Tower(int i) {
			this.disks = new Stack<Integer>();
		}
		
		public void add(int d) {
			if (!disks.isEmpty() && disks.peek() <= d) {
				System.out.println("Error placing disk" + d);
			}
			else {
				disks.push(d);
			}
		}
		
		public void moveTopTo(Tower t) {
			int top = disks.pop();
			t.add(top);
		}

		public void moveDisks(int n, Tower destination, Tower buffer) {
			if (n > 0) {
				moveDisks(n-1, buffer, destination);
				moveTopTo(destination);
				buffer.moveDisks(n-1, destination, this);
			}
		}
		
		public void printTower() {
			System.out.println(Arrays.toString(disks.toArray()));
		}
	}
	
	public void solveHanoi(int n) {
		Tower[] towers = new Tower[3];
		for(int i = 0; i < 3; i++) {
			towers[i] = new Tower(i);
		}
		
		for(int i = n - 1; i >=0; i--) {
			towers[0].add(i);
		}
		towers[0].printTower();
		towers[2].printTower();
		towers[0].moveDisks(n, towers[2], towers[1]);
		towers[0].printTower();
		towers[2].printTower();
	}
	
	public static void main(String[] args) {
		Question6_JB obj = new Question6_JB();
		obj.solveHanoi(5);
	}
}