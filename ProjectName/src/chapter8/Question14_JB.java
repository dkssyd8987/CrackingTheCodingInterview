package chapter8;

public class Question14_JB {
	public int countEval(String str, boolean bool) {
		int strLength = str.length();
		if(strLength == 0) {
			return 0;
		}
		else if(strLength == 1) {
			return stringToBool(str) == bool ? 1 : 0;
		}
		
		int ways = 0;
		
		for(int i = 1; i < strLength; i+=2) {
			char c = str.charAt(i);
			String left = str.substring(0, i);
			String right = str.substring(i+1);
			
			int leftTrue = countEval(left, true);
			int leftFalse = countEval(left, false);
			int rightTrue = countEval(right, true);
			int rightFalse = countEval(right, false);
			
			int total = (leftTrue + leftFalse) * (rightTrue + rightFalse);
			
			int totalTrue = 0;
			if (c == '^') {
				totalTrue = leftTrue * rightFalse + leftFalse * rightTrue;
			}
			else if (c == '&') {
				totalTrue = leftTrue * rightTrue;
			}
			else if (c == '|') {
				totalTrue = leftTrue * rightTrue + leftFalse * rightTrue + leftTrue * rightFalse;
			}
			
			int subWays = bool ? totalTrue : total - totalTrue;
			ways += subWays;
		}
		
		return ways;
	}
	
	boolean stringToBool(String c) {
		return c.equals("1") ? true : false;
	}
	
	public static void main(String[] args) {
		Question14_JB obj = new Question14_JB();
		System.out.println(obj.countEval("0&0&0&1^1|0", true));
	}
}
