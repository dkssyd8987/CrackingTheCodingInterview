package chapter8;

public class Question3_Advanced_JB {
	public boolean hasMagicIndex(int[] array) {
		return hasMagicIndex(array, 0, array.length-1);
	}
	public boolean hasMagicIndex(int[] array, int start, int end) {
		if(end == start) {
			if(array[start] == start) {
				return true;
			}
			else {
				return false;
			}
		}
		int mid = (start+end) / 2;
		// distinct 하지 않은 경우
		// array[start] > end 이거나
		// array[end] < start 라면
		// 아무리 숫자가 중복되더라도 해당 index 사이에 magic index 가 있을 수 없음.
		if (array[start] > end) {
			return false;
		}
		else if(array[end] < start) {
			return false;
		}
		else {
			return (hasMagicIndex(array, start, mid) || hasMagicIndex(array, mid+1, end));
		}
	}
	
	public static void main(String[] args) {
		Question3_Advanced_JB obj = new Question3_Advanced_JB();
		int[] temp = new int[5];
		temp[0] = -3;
		temp[1] = 0;
		temp[2] = 1;
		temp[3] = 1;
		temp[4] = 10;
		System.out.println(obj.hasMagicIndex(temp));
	}

}
