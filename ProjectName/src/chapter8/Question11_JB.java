package chapter8;

import java.util.Arrays;

public class Question11_JB {
	public int countWays(int cents) {
		int count = 0;
		// 속도 증가를 위해 cents 별 개수 저장
		int[] memo = new int[cents+1];
		Arrays.fill(memo, -1);
		count = countWays(cents, memo);
		return count;
	}
	
	public int countWays(int cents, int[] memo) {
		// k cent 에 해당하는 동전이 있다면
		// n cents 를 내는 방법의 수는 (n-k) cents 를 나타내는 방법의 수 * k cents 를 나타내는 방법의 수
		// k cents 를 나타내는 방법의 수는
		// k cent 짜리 동전 내기 + k-1 cents 를 내는 방법의 수 이므로
		// k-1 cents 를 내는 방법의 수 + 1
		if(memo[cents] == -1) {
			if(cents / 25 >= 1) {
				memo[cents] = countWays(cents-25,memo) * (countWays(24,memo)+1);
			}
			else if(cents / 10 >= 1) {
				memo[cents] = countWays(cents-10, memo) * (countWays(9, memo)+1);
			}
			else if(cents / 5 >= 1) {
				memo[cents] = countWays(cents-5, memo) * (countWays(4, memo)+1);
			}
			else {
				memo[cents] = 1;
			}
		}
		return memo[cents];
	}
	
	public static void main(String[] args) {
		Question11_JB obj = new Question11_JB();
		System.out.println(obj.countWays(20));
	}
}
