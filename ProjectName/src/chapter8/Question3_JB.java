package chapter8;

public class Question3_JB {
	public boolean hasMagicIndex(int[] array) {
		return hasMagicIndex(array, 0, array.length-1);
	}
	public boolean hasMagicIndex(int[] array, int start, int end) {
		// array 를 쪼개서 확인하는 작업을 반복
		// end == start 인 경우 magic index 인지 여부를 확인하여 return
		if(end == start) {
			if(array[start] == start) {
				return true;
			}
			else {
				return false;
			}
		}
		int mid = (start+end) / 2;
		// distinct 한 경우
		// start index 의 array 값이 start 보다 커버리면
		// sorted 된 array 에서는 index 증가할수록 값이 커질 것이므로
		// 그 사이에 magic index 존재 불가
		// end index 의 array 값이 end 보다 작은 경우에도 마찬가지
		if(array[start] > start) {
			return false;
		}
		else if (array[end] < end) {
			return false;
		}
		else {
			return (hasMagicIndex(array, start, mid) || hasMagicIndex(array, mid+1, end));
		}
	}
	
	public static void main(String[] args) {
		Question3_JB obj = new Question3_JB();
		int[] temp = new int[5];
		temp[0] = -3;
		temp[1] = 1;
		temp[2] = 2;
		temp[3] = 4;
		temp[4] = 10;
		System.out.println(obj.hasMagicIndex(temp));
	}
}
