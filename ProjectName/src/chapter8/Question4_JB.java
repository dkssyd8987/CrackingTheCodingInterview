package chapter8;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class Question4_JB {
	public HashSet<HashSet<Integer>> getSubsets(HashSet<Integer> set) {
		HashSet<HashSet<Integer>> list = new HashSet<HashSet<Integer>>();
		getSubsets(list, set);
		return list;
	}
	public void getSubsets(HashSet<HashSet<Integer>> list, HashSet<Integer> set) {
		if(set.size() == 1) {
			// set 의 크기가 1이라면 그 자체를 추가하면 됨.
			list.add(set);
		}
		else {
			// 이미 여기에 진입하면 subset 에 해당하므로 add 는 해줌.
			list.add(set);
			for (Integer item : set) {
				// set 의 item 에 for each 를 하고
				// 해당 item 을 지운 새로운 set은 set 의 subset
				HashSet<Integer> tempSet = new HashSet<Integer>();
				tempSet = (HashSet)set.clone();
				tempSet.remove(item);
				getSubsets(list,  tempSet);
			}
		}
	}
	
	
	public static void main(String[] args) {
		Question4_JB obj = new Question4_JB();
		HashSet<Integer> set = new HashSet<Integer>();
		set.add(1);
		set.add(2);
		set.add(3);
//		set.add(4);
		HashSet<HashSet<Integer>> list = obj.getSubsets(set);
		System.out.println(list);
	}
}
