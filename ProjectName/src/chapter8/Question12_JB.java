package chapter8;

import java.util.ArrayList;
import java.util.Arrays;

public class Question12_JB {
	public ArrayList<Integer[]> emplaceQueens(int size) {
		ArrayList<Integer[]> answer = new ArrayList<Integer[]>();
		emplaceQueens(size, answer);
		return answer;
	}
	
	public void emplaceQueens(int size, ArrayList<Integer[]> answer) 
	{
		Integer[] queens = new Integer[size];
		Arrays.fill(queens, -99);
		emplaceQueens(0, size, queens, answer);
	}
	
	public void emplaceQueens(int row, int size, Integer[] queens, ArrayList<Integer[]> answer)
	{
		if(row  == size) {
			answer.add(queens.clone());
		}
		else {
			for(int i = 0; i < size; i++) {
				if(isValidPos(row, i, queens)) {
					queens[row] = i;
					emplaceQueens(row+1,size,queens,answer);
				}
			}
		}
	}
	
	public boolean isValidPos(int row, int col, Integer[] queens) {
		for (int i = 0; i < row; i++) {
			int col2 = queens[i];
			
			if(col2 == col) {
				return false;
			}
			
			if((row - col) == (i - col2) ||
					(row+col) == (i+col2)) {
				return false;
			}
		}
		return true;
	}
	
	public void printAnswer(ArrayList<Integer[]> answer) {
		for(Integer[] queens : answer) {
			for(int i = 0 ; i < queens.length; i++) {
				System.out.print("("+i+","+queens[i]+")");
			}
			System.out.println();
		}
	}
	
	public static void main(String[] args) {
		Question12_JB obj = new Question12_JB();
		ArrayList<Integer[]> answer = obj.emplaceQueens(8);
		obj.printAnswer(answer);
	}
}
