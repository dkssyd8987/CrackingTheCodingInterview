package chapter5;

public class Question7 {
	int PairwiseSwap(int num) {
		String str = Integer.toBinaryString(num);
		int length = str.length();
		int evenBit = 0;
		int oddBit = 0;
		for(int i = 0; i < length; i++) {
			if(i % 2 == 0) {
				oddBit += Math.pow(2, i);
			}
			else {
				evenBit += Math.pow(2, i);
			}
		}
		System.out.println(Integer.toBinaryString(evenBit));
		System.out.println(Integer.toBinaryString(num&evenBit));
		return ((num & evenBit)>>1) + ((num & oddBit)<<1);
	}
	public static void main(String[] args) {
		Question7 obj = new Question7();
		System.out.println(Integer.toBinaryString(17571));
		System.out.println(Integer.toBinaryString(obj.PairwiseSwap(17571)));
	}
}
