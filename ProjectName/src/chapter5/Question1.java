package chapter5;

public class Question1 {
	int InsertNum(int target, int num, int startBit, int endBit) {
		// 타겟의 i th 부터 j th 비트를 먼저 클리어한다
		// i 부터 j 까지 0 인 (ex. 11000011111) bit 를 만들고 target 과 AND 연산
		int mask = ~(-1 << endBit) ^ (-1 << startBit);
		target = target & mask;
		// 삽입하고자 하는 num 을 j 만큼 shift 해주고
		// 삽입 위치가 clear 된 target 과 or 연산
		return target | (num << startBit);
	}
	public static void main(String[] args) {
		Question1 obj = new Question1();
		int x = 32445;
		int y = 9;
		int i = 2;
		int j = 5;
		System.out.println(Integer.toBinaryString(x));
		System.out.println(Integer.toBinaryString(y));
		System.out.println(Integer.toBinaryString(obj.InsertNum(x,  y,  i,  j)));
	}
}

