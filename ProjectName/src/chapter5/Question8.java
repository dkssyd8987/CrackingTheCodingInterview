package chapter5;

public class Question8 {
	void drawLine(byte[] screen, int width, int x1, int x2, int y) {
		// 좌측 하단이 (0,0) 이라고 생각하고, byte array 역시 좌측 하단부터 오른쪽, 위 순서로 되어있다고 생각
		if((x1 > width) | (x2 > width)) {
			System.out.println("Error");
			return;
		}
		// 층마다 쌓이는 개수에 y 를 곱하고, x1, x2 좌표를 통해 첫 byte index 를 찾음
		int firstIndex = (width/8)*y + (x1/8);
		int secondIndex = (width/8)*y + (x2/8);
		// 1111111 을 shift 하여 필요한 만큼만 1을 채운다
		screen[firstIndex] = (byte)(0xFF >>> (x1%8 - 1));
		System.out.println(byteToBinaryString(screen[firstIndex]));
		screen[secondIndex] = (byte)(0xFF << (x2%8));
		System.out.println(byteToBinaryString(screen[secondIndex]));
	}
	public String byteToBinaryString(byte n) {
	    StringBuilder sb = new StringBuilder("00000000");
	    for (int bit = 0; bit < 8; bit++) {
	        if (((n >> bit) & 1) > 0) {
	            sb.setCharAt(7 - bit, '1');
	        }
	    }
	    return sb.toString();
	}
	public static void main(String[] args) {
		Question8 obj = new Question8();
		byte[] test = new byte[100];
		obj.drawLine(test, 32, 20,  28, 1);
	}
}
