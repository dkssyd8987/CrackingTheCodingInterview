package chapter5;

public class Question6 {
	int CountFlips(int target, int num) {
		// 두 숫자를 XOR 연산 이후 1의 개수를 세면 flip 해야하는 비트의 개수가 나온다
		int xor = target ^ num;
		String str = Integer.toBinaryString(xor);
		char[] charArray = str.toCharArray();
		int count = 0;
		for(int i = 0; i < charArray.length; i++) {
			if(charArray[i] == '1') {
				count++;
			}
		}
		return count;
	}
	public static void main(String[] args) {
		Question6 obj = new Question6();
		System.out.println(obj.CountFlips(29, 15));
	}
}
