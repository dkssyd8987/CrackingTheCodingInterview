package chapter10;

public class Question4_JB {
	public class Listy {
		private int[] array;
		
		public Listy() {}
		
		public Listy(int[] array) {
			this.array = array;
		}
		
		public int elementAt(int i) {
			if((i < 0) || (i >= array.length))
				return -1;
			return array[i];
		}
	}
	
	static public int findIndex(Listy listy, int target)
	{
		int ret = -1;
		int i = 1;
		while(listy.elementAt(i) != -1) {
			i *= 2;
		}
		int length = i;
		ret = binarySearch(listy, 0, length, target);
		return ret;
	}
	
	static public int binarySearch(Listy listy, int start, int end, int target)
	{
		int mid = (start + end) / 2;
		int ret = -1;
		if(listy.elementAt(mid) > target) {
			ret = binarySearch(listy, start, mid-1, target);
		}
		else if(listy.elementAt(mid) < target){
			ret = binarySearch(listy, mid + 1, end, target);
		}
		else {
			ret = mid;
		}
		return ret;
	}
	
	public static void main(String[] args) {
		Question4_JB obj = new Question4_JB();
		int[] test = new int[] {1, 4, 5, 6, 7, 9, 11, 13, 15, 17};
		Listy test2 = obj.new Listy(test);
		System.out.println(findIndex(test2, 9));
	}
}
