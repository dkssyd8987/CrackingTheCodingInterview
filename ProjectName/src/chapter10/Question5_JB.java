package chapter10;

public class Question5_JB {
	public int sparseSearch(String[] array, int start, int end, String target)
	{
		int mid = (start+end)/2;
		int ret = -1;
		if(array[mid].isEmpty()) {
			while(true) {
				++mid;
				if(!array[mid].isEmpty()) {
					// 문자열이 있는 곳을 기준으로 삼음.
					break;
				}
				if(mid == end) {
					// 끝까지 공백만 나오면 -1 return
					return -1;
				}
			}
		}
		
		// compareTo 가 음수이면 mid, target 순이 알파벳순서 -> 오른쪽 찾아봐야 됨.
		if(array[mid].compareTo(target) == 0) {
			ret = mid;
		}
		else if(array[mid].compareTo(target) < 0) {
			ret = sparseSearch(array, mid+1, end, target);
		}
		else {
			ret = sparseSearch(array, start, mid-1, target);
		}
		return ret;
	}
	
	public static void main(String[] args) {
		Question5_JB obj = new Question5_JB();
		String[] test = new String[] {"at", "", "", "", "ball", "", "", "car", "", "", "dad", "", "" };
		System.out.println(obj.sparseSearch(test, 0, test.length-1, "ball"));
	}
}
