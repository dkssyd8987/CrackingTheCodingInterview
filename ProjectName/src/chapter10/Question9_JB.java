package chapter10;

public class Question9_JB {
	public class intPair {
		private int x;
		private int y;
		
		public intPair(int x, int y) {
			this.x = x;
			this.y = y;
		}
		
		public int x() {
			return this.x;
		}
		
		public int y() {
			return this.y;
		}
		
		public void setX(int x) {
			this.x = x;
		}
		
		public void setY(int y) {
			this.y = y;
		}
	}
	
	public intPair sortedMatrixSearch(int[][] matrix, int target)
	{
		intPair ret = new intPair(-1, -1);
		ret = sortedMatrixSearch(matrix, new intPair(0, 0), new intPair(matrix[0].length -1, matrix.length-1), target);
		return ret;
	}
	
	public intPair sortedMatrixSearch(int[][] matrix, intPair start, intPair end, int target)
	{
		intPair ret = new intPair(-1, -1);
		int midX = (start.x + end.x)/2;
		int midY = (start.y + end.y)/2;
		if(matrix[midX][midY] > target) {
			ret = sortedMatrixSearch(matrix, start, new intPair(midX, midY), target);
		}
		else if (matrix[midX][midY] < target) {
			ret = sortedMatrixSearch(matrix, new intPair(midX, midY), end, target);
		}
		else {
			ret = new intPair(midX, midY);
		}
		return ret;
	}
	
	public static void main(String[] args) {
		Question9_JB obj = new Question9_JB();
		int[][] test = new int[][] {{1, 2, 3} , {4, 5, 6}};
		System.out.println(test.length);
		System.out.println(test[0].length);
	}
}
