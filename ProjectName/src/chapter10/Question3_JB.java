package chapter10;

public class Question3_JB {
	public int findIndex(int[] array, int left, int right, int target) {
		int mid = (left+right) / 2;
		if(array[mid] == target)
			return mid;
		int ret = 0;
		if(array[left] < array[mid]) {
			// left 부터 mid 까지 순서대로 되어있음
			if(array[mid] > target) {
				// mid 가 target 보다 크면
				// left 보다 target 이 작으면 mid 오른쪽에, 아니면 왼쪽에 존재
				if(array[left] > target)  {
					ret = findIndex(array, mid, right, target);
				}
				else {
					ret = findIndex(array, left, mid, target);					
				}
			}
			else {
				// mid 가 target 보다 작으면 무조건 오른쪽에 존재
				ret = findIndex(array, mid, right, target);
			}
		}
		else {
			// mid 부터 right 까지 순서대로 되어있음
			if(array[mid] > target) {
				// mid 가 target 보다 크면 무조건 왼쪽에 존재
				ret = findIndex(array, left, mid, target);
			}
			else {
				// mid 가 target 보다 작으면
				// right 보다 target 이 크면 왼쪽에, 아니면 오른쪽에 존재
				if(array[right] < target) {
					ret = findIndex(array, left, mid, target);
				}
				else {
					ret = findIndex(array, mid, right, target);
				}
			}
		}
		
		return ret;
	}
	
	public static void main(String[] args) {
		Question3_JB obj = new Question3_JB();
		int[] array = new int[] {15, 16, 19, 20, 25,26, 1, 3, 4, 5, 7, 10, 14};
		System.out.println(obj.findIndex(array, 0, array.length-1, 5));
	}
}
