package chapter10;

public class Question1_JB {
	public void mergeArrays(int[] target, int[] array, int targetSize)
	{
		int[] buffer = target.clone();
		int bufferIndex = 0;
		int arrayIndex = 0;
		int targetIndex = 0;
		while(true) {
			if(bufferIndex == targetSize) {
				while(arrayIndex < array.length) {
					target[targetIndex++] = array[arrayIndex++];
				}
				break;
			}
			else if(arrayIndex == array.length) {
				while(bufferIndex < targetSize) {
					target[targetIndex++] = buffer[bufferIndex++];
				}
				break;
			}
			if(buffer[bufferIndex] < array[arrayIndex]) {
				target[targetIndex++] = buffer[bufferIndex++];
			}
			else {
				target[targetIndex++] = array[arrayIndex++];
			}
		}
	}
	
	public static void main(String[] args) {
		Question1_JB obj = new Question1_JB();
		int[] target = new int[10];
		target[0] = 2;
		target[1] = 4;
		target[2] = 6;
		int[] array = new int [] {1,3,5,7};
		obj.mergeArrays(target, array, 3);
		for(int i = 0; i < target.length; ++i) {
			System.out.println(target[i]);
		}
	}
}
