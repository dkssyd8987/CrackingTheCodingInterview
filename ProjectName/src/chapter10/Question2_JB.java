package chapter10;

import java.util.Arrays;
import java.util.Comparator;

public class Question2_JB {
	class anagramComparator implements Comparator<String> {
		public String sortString(String str) {
			char[] cArray = str.toCharArray();
			Arrays.sort(cArray);
			String ret = new String(cArray);
			return ret;
		}
		
		public int compare(String str1, String str2) {
			return sortString(str2).compareTo(sortString(str1));
		}
	}
	public void sortStringByAnagram(String[] strArray) {
		Arrays.sort(strArray, new anagramComparator());
	}
	
	public static void main(String[] args) {
		Question2_JB obj = new Question2_JB();
		String[] test = new String[] { "asdf", "fdas", "acdc", "cdca", "bcsa", "ascb" };
		obj.sortStringByAnagram(test);
		for(int i = 0; i < test.length; ++i) {
			System.out.println(test[i]);			
		}
	}
}
