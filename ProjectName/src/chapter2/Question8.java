package chapter2;

public class Question8 {
	CJBLinkedList.Node LoopDetection(CJBLinkedList ll) {
		CJBLinkedList.Node n = ll.head;
		CJBLinkedList.Node ref = ll.head;
		while(n.next != null && ref.next.next != null) {
			n = n.next;
			ref = ref.next.next;
			if(n == ref) {
				break;
			}
		}
		return n;
	}
	public static void main(String[] args) {
		Question8 obj = new Question8();
		CJBLinkedList test = new CJBLinkedList();
		CJBLinkedList.Node n1 = test.new Node(1);
		CJBLinkedList.Node n2 = test.new Node(2);
		CJBLinkedList.Node n3 = test.new Node(3);
		CJBLinkedList.Node n4 = test.new Node(4);
		CJBLinkedList.Node n5 = test.new Node(5);
		test.appendNode(n1);
		test.appendNode(n2);
		test.appendNode(n3);
		test.appendNode(n4);
		test.appendNode(n3);
		System.out.println(obj.LoopDetection(test).data);
	}
}
