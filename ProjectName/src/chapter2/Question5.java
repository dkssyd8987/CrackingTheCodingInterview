package chapter2;

public class Question5 {
	CJBLinkedList SumLists(CJBLinkedList l1, CJBLinkedList l2, boolean reversed) {
		CJBLinkedList res = new CJBLinkedList();
		CJBLinkedList.Node n1 = l1.head;
		CJBLinkedList.Node n2 = l2.head;
		int num1 = 0;
		int num2 = 0;
		if(!reversed) {
			int dim = 1;
			for(int j = 0; j < l1.size() - 1; j++) {
				dim *= 10;
			}
			while(n1 != null) {
				num1 += n1.data*dim;
				n1 = n1.next;
				dim = dim/10;
			}
			dim = 1;
			for(int j = 0; j < l2.size() - 1; j++) {
				dim *= 10;
			}
			while(n2 != null) {
				num2 += n2.data*dim;
				n2 = n2.next;
				dim = dim/10;
			}
		}
		else {
			int dim = 1;
			while(n1 != null) {
				num1 += n1.data*dim;
				n1 = n1.next;
				dim *= 10;
			}
			dim = 1;
			while(n2 != null) {
				num2 += n2.data*dim;
				dim *= 10;
				n2 = n2.next;
			}
		}
		int result = num1+num2;
		String temp = Integer.toString(result);
		if(reversed) {
			for(int i = temp.length() - 1; i>=0; i--) {
				res.appendToTail(temp.charAt(i) - '0');
			}
		}
		else {
			for(int i = 0; i < temp.length(); i++) {
				res.appendToTail(temp.charAt(i) - '0');
			}
		}
		return res;
	}
	public static void main(String[] args) {
		Question5 obj = new Question5();
		CJBLinkedList test = new CJBLinkedList();
		for(int i = 0; i < 4; i++) {
			test.appendToTail(i+1);
		}
		obj.SumLists(test,test, true).printList();
		test.printList();
	}
}
