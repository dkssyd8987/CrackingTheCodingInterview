package chapter2;

public class Question4 {
	CJBLinkedList partition(CJBLinkedList ll, int partition) {
		CJBLinkedList left = new CJBLinkedList();
		CJBLinkedList right = new CJBLinkedList();
		CJBLinkedList.Node n = ll.head;
		while(n != null) {
			if(n.data >= partition) {
				right.appendToTail(n.data);
			}
			else {
				left.appendToTail(n.data);
			}
			n = n.next;
		}
		CJBLinkedList.Node rightN = right.head;
		while(rightN != null) {
			left.appendToTail(rightN.data);
			rightN = rightN.next;
		}
		return left;
	}
	public static void main(String[] args) {
		Question4 obj = new Question4();
		CJBLinkedList test = new CJBLinkedList();
		for (int i = 0; i < 10; i ++) {
			test.appendToTail((int)(Math.random()*100));
		}
		test.printList();
		obj.partition(test, 50).printList();
	}
}
