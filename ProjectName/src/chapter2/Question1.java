package chapter2;

public class Question1 {
	void removeDups(CJBLinkedList ll) {
		CJBLinkedList.Node n = ll.head;
		while(n != null) {
			int check = n.data;
			CJBLinkedList.Node ref = n;
			while(ref != null) {
				CJBLinkedList.Node refN = ref.next;
				if(refN == null) {
					break;
				}
				if(refN.data == check) {
					ref.next = ref.next.next;
				}
				else {
					ref = ref.next;
				}
			}
			n = n.next;
		}
	}
	public static void main(String[] args) {
		Question1 obj = new Question1();
		CJBLinkedList test = new CJBLinkedList();
		test.appendToTail(3);
		test.appendToTail(5);
		test.appendToTail(3);
		test.appendToTail(4);
		test.appendToTail(3);
		test.appendToTail(4);
		test.printList();
		System.out.println(" ");
		System.out.println("=====");
		obj.removeDups(test);
		test.printList();
	}
}
