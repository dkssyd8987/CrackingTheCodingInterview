package chapter2;

public class Question3 {
	void deleteMiddleNode(CJBLinkedList ll) {
		CJBLinkedList.Node n = ll.head;
		for(int i = 1; i < (int)(ll.size()/2); i++) {
			n = n.next;
		}
		n.next = n.next.next;
	}
	
	public static void main(String[] args) {
		Question3 obj = new Question3();
		CJBLinkedList test = new CJBLinkedList();
		for(int i = 0; i < 6; i++) {
			test.appendToTail(i);
		}
		test.printList();
		obj.deleteMiddleNode(test);
		test.printList();
	}
}
