package chapter2;

import chapter2.CJBLinkedList.Node;

public class Question7 {
	boolean checkIntersection(CJBLinkedList l1, CJBLinkedList l2) {
		CJBLinkedList.Node n1 = l1.head;
		while(n1 != null) {
			CJBLinkedList.Node n2 = l2.head;
			while(n2 != null) {
				if(n1 == n2) {
					return true;
				}
				n2 = n2.next;
			}
			n1 = n1.next;
		}
		return false;
	}
	public static void main(String[] args) {
		Question7 obj = new Question7();
		CJBLinkedList test = new CJBLinkedList();
		CJBLinkedList test2 = new CJBLinkedList();
		CJBLinkedList.Node n1 = test.new Node(1);
		CJBLinkedList.Node n2 = test.new Node(2);
		CJBLinkedList.Node n3 = test.new Node(3);
		CJBLinkedList.Node n4 = test.new Node(4);
		CJBLinkedList.Node n5 = test.new Node(5);
		test.appendNode(n1);
		test.appendNode(n2);
		test.appendNode(n3);
		test2.appendNode(n4);
		test2.appendNode(n5);
		test.printList();
		test2.printList();
		System.out.println(obj.checkIntersection(test,  test2));
	}
}
