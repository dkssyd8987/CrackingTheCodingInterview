package chapter2;

public class Question6 {
	boolean checkPalindrome(CJBLinkedList l1, CJBLinkedList l2) {
		if(l1.size() != l2.size()) {
			return false;
		}
		int[] list1 = new int[l1.size()];
		int[] list2 = new int[l2.size()];
		CJBLinkedList.Node n1 = l1.head;
		CJBLinkedList.Node n2 = l2.head;
		int i = 0;
		while(n1 != null) {
			list1[i] = n1.data;
			i++;
			n1 = n1.next;
		}
		i -= 1;
		while(n2 != null) {
			list2[i] = n2.data;
			i--;
			n2 = n2.next;
		}
		for(int j = 0; j < list1.length; j++) {
			if(list1[j] != list2[j]) {
				return false;
			}
		}
		return true;
	}
	public static void main(String[] args) {
		Question6 obj = new Question6();
		CJBLinkedList test1 = new CJBLinkedList();
		CJBLinkedList test2 = new CJBLinkedList();
		for(int i = 0; i < 5; i++) {
			test1.appendToTail(i);
		}
		for(int i = 5; i >= 1; i--) {
			test2.appendToTail(i);
		}
		System.out.println(obj.checkPalindrome(test1,  test2));
	}
}
