package chapter2;

public class CJBLinkedList {
   
   public Node head = null;
   
   public Node deleteNode(Node head, int d){
      Node n = head;
      if(n.data == d){
         return head.next;
      }
      while(n.next!=null){
         if(n.next.data == d){
            n.next = n.next.next;
            return head;
         }
         n = n.next;
      }
      return head;
   }
   public void appendToTail(int d){
      Node end = new Node(d);
      if(this.head == null){
         this.head = end;
         return;
      }
      Node n = this.head;
      while(n.next != null){
         n = n.next;
      }
      n.next = end;
   }
   
   public void appendNode(Node n) {
	   if(this.head == null) {
		   this.head = n;
		   return;
	   }
	   Node existN = this.head;
	   while(existN.next != null) {
		   existN = existN.next;
	   }
	   existN.next = n;
   }
   
   public void printList(){
      Node n = this.head;
      while(n != null){
         System.out.print(n.data+"->");
         n = n.next;
      }
      System.out.println(" ");
      System.out.println("===");
   }
   
   public int size() {
	   int size = 1;
	   Node n = this.head;
	   while(n.next != null) {
		   n = n.next;
		   size++;
	   }
	   return size;
   }
   
   public class Node{
      Node next = null;
      int data;
      
      public Node(int d){
         data = d;
      }
      
      
   }
}