package chapter7;

import java.util.ArrayList;

public class Question5_JB {
	enum Genre {
		LOVE, HORROR, DETECTIVESTORY, UNKNOWN;
	}
	class OnlineBookReader {
		
	}
	class OnlineBook {
		private int pages;
		private Genre genre;
		public OnlineBook(int pages, Genre genre) {
			this.pages = pages;
			this.genre = genre;
		}
	}
	class BookList {
		private ArrayList<OnlineBook> booklist;
		public void addBook(OnlineBook book) {
			this.booklist.add(book);
		}
	}
}
