package chapter7;

import java.util.ArrayList;

public class Question4_JB {
	enum Color {
		BLUE, RED, WHITE, BLACK, YELLOW, ETC;
	}
	class ParkingLot {
		private ArrayList<Seat> seats;
		public ParkingLot(int numberOfSeats) {
			this.seats = new ArrayList<Seat>(numberOfSeats);
		}
		public boolean isFull() {
			for(int i = 0; i < this.seats.size(); i++) {
				if(this.seats.get(i).isAvailable() == true) {
					return false;
				}
			}
			return true;
		}
		public Seat getAvailableSeat() {
				for(int i = 0; i < this.seats.size(); i++) {
					if(this.seats.get(i).isAvailable() == true) {
						return this.seats.get(i);
					}
				}
				return null;
			}
		public void parkCar(Car car) {
			Seat availableSeat = this.getAvailableSeat();
			if(availableSeat == null) {
				System.out.println("parking lot is full");
				return;
			}
			availableSeat.parkCar(car);
		}
	}
	class Seat {
		private Car car = null;
		public void parkCar(Car car) {
			this.car = car;
		}
		public boolean isAvailable() {
			if(this.car == null) {
				return true;
			}
			return false;
		}
	}
	class Car {
		private Color color;
		private int licensePlate;
		public Car(Color color, int lp) {
			this.color = color;
			this.licensePlate = lp;
		}
	}
}