package chapter7;

import java.util.ArrayList;

import chapter5.Question8;

public class Question1_JB {
	public enum ShapeType {
		SPADES, HEART, DIAMOND, CLUB
	}
	public class Card {
		private ShapeType type;
		private int number;
		public Card(int num, ShapeType shape) {
			this.number = num;
			this.type = shape;
		}
		public int getNum(boolean t) {
			if(t == true) {
				if (this.number == 1) {
					return 14;
				}
			}
			return this.number;
		}
		public ShapeType getShape() {
			return this.type;
		}
	}
	
	public class DeckOfCards {
		public ArrayList<Card> cards;
		public void init() {
			for(int i = 1; i < 14; i++) {
				cards.add(new Card(i, ShapeType.CLUB));
				cards.add(new Card(i, ShapeType.DIAMOND));
				cards.add(new Card(i, ShapeType.SPADES));
				cards.add(new Card(i, ShapeType.HEART));
			}
			this.shuffle();
		}
		public void shuffle() {
			// Arraylist 순서 섞어줄 수 있는 코드
		}
		public Card deal() {
			if(cards.size() == 0) {
				this.init();
			}
			return cards.remove(0);
		}
	}
	
	public static void main(String[] args) {
		Question1_JB obj = new Question1_JB();
		DeckOfCards deck = obj.new DeckOfCards();
		deck.init();
		// deck.deal() 로 card 를 받아서 모으고
		// 해당 card.getNum() 의 합을 구해서 21 기준으로 blackjack 수행 가능.
		// Ace 를 14 로 쓰고 싶으면 card.getNum(true), 그 외의 경우에는 card.getNum(false) 로 사용가능.
	}
}
