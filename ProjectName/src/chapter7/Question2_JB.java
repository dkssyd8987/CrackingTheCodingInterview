package chapter7;

public class Question2_JB {
	abstract class Employee {
		protected int rank = 0;
		private Call call = null;
		
		public void setCall(Call call) {
			this.call = call;
		}
		public boolean isAvailable() {
			if(this.call == null) {
				return true;
			}
			return false;
		}
	}
	
	class Respondent extends Employee {
		public Respondent() {
			this.rank = 1;
		}
	}
	
	class Manager extends Employee {
		public Manager() {
			this.rank = 2;
		}
	}
	
	class Director extends Employee {
		public Director() {
			this.rank = 3;
		}
	}
	
	class Call {
		private boolean handling;
		public void setHandling(boolean handling) {
			this.handling = handling;
		}
	}
	
	void dispatchCall(Call call, Employee handler) {
		if(handler.isAvailable()) {
			handler.setCall(call);
		}
	}
}
